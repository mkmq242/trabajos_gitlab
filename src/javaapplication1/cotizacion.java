/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;



/**
 *
 * @author Ulises
 */
public class cotizacion {
    // Atributos de la clase
    
    private String descripAuto;
    private double precio;
    private double porcentajeImpuesto;
    private int plazo;
    private int nucotizacion;

    // constructores
    public cotizacion() {
        
        this.nucotizacion=0;
        this.descripAuto = "";
        this.precio = 0.0;
        this.porcentajeImpuesto = 0.0;
        this.plazo = 0;
    }

    public cotizacion(int numCotizacion, String descripAuto, double precio, double porcentajeImpuesto, int plazo) {
        
        this.descripAuto = descripAuto;
        this.precio = precio;
        this.porcentajeImpuesto = porcentajeImpuesto;
        this.plazo = plazo;
    }


      public int getNucotizacion() {
        return nucotizacion;
    }

    public void setNucotizacion(int nucotizacion) {
        this.nucotizacion = nucotizacion;
    }
    
   
    public String getDescripAuto() {
        return descripAuto;
    }

    public void setDescripAuto(String descripAuto) {
        this.descripAuto = descripAuto;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public double getPorcentajeImpuesto() {
        return porcentajeImpuesto;
    }

    public void setPorcentajeImpuesto(double porcentajeImpuesto) {
        this.porcentajeImpuesto = porcentajeImpuesto;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }

    // metodos de comportamiento
    public double calcularPagoImpuesto() {
        // calcula el impuesto con el precio y el porcentaje de impuesto
        return precio * (porcentajeImpuesto / 100);
    }

    public double calcularFinanciamiento() {
        // calcula el financiamiento restando el impuesto del precio
        return precio - calcularPagoImpuesto();
    }

    public double calcularMensualidad() {
        // calcula la mensualidad dividiendo el financiamiento entre el plazo
        return calcularFinanciamiento() / plazo;
    }
}
